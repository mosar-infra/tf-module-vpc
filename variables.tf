# networking/variables.tf

variable "public_subnet_count" {}
variable "private_subnet_count" {}
variable "vpc_cidr" {}
variable "public_cidrs" {}
variable "private_cidrs" {}
variable "max_subnets" {}
variable "create_natgw" {}
variable "environment" {}
variable "managed_by" {}
