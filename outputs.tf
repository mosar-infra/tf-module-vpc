output "vpc" {
  value = aws_vpc.vpc
}

output "public_subnet" {
  value = aws_subnet.public_subnet
}

output "private_subnet" {
  value = aws_subnet.private_subnet
}

output "private_subnet_cidrs" {
  value = aws_subnet.private_subnet.*.cidr_block
}

output "public_subnet_cidrs" {
  value = aws_subnet.public_subnet.*.cidr_block
}

output "internet_gateway" {
  value = aws_internet_gateway.igw
}

output "nat_gateway" {
  value = aws_nat_gateway.nat_gw
}

